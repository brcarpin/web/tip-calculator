const button = document.querySelector("button");  // 
const output = document.querySelector(".output");
console.log(button);
button.addEventListener("click",function()
{
    const cost = document.querySelector("#cost");
    const percentage = document.querySelector("#tip-percentage");
    let tip = (Number(cost.value) * (percentage.value / 100));
    let final = Number(cost.value) + tip;
    console.log(tip);
    let temp = `<h1>You should tip $${tip} on $${cost.value}</h1>`;
    let bill = `<h1>Final Bill amount: $${final.toFixed(2)}</h1>`;
    output.innerHTML = temp + bill;
});